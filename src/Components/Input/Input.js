import React from 'react';
import './Input.css';
import {Tags} from './../Tags/Tags'

export class Input extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            showList: true
        }

        this.showList = this.showList.bind(this);
        this.closeList = this.closeList.bind(this);
    }

    showList(event){

        event.preventDefault();
        this.setState({ showList: false }, () => {
            document.addEventListener('click', this.closeList);
        });
    }

    closeList(event){
        if (!this.dropdownList.contains(event.target) && !this.input.contains(event.target)) {

            this.setState({ showList: true }, () => {
                document.removeEventListener('click', this.closeList);
            });

        }
    }

    render() {
        return(
            <div className='Input-wrapper'>
                <Tags
                    tags={this.props.tags}
                    color={this.props.color}
                    deleteTag={tag => this.props.deleteTag(tag)}/>
                <span>Tags: </span>
                <div className="Input__inner">
                    <input
                        type='text'
                        className='Input'
                        onFocus={this.showList}
                        onChange={(e) => this.props.searchTags(e)}
                        value={this.props.value}
                        ref={(element) => {this.input = element;}}
                    />
                <div className='Input__list' hidden={this.state.showList} ref={(element) => {this.dropdownList = element;}}>
                        {
                            this.props.initialTags.map((tag, index) =>
                                <div key={index} onClick={(e) =>
                                    {
                                      this.props.addTags(tag);
                                      this.setState({ showList: true })
                                    }
                                }>{tag}</div>
                            )
                        }
                    </div>
                </div>
            </div>
        )
    }
}
