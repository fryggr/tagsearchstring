import React from 'react';
import "./Tags.css"

export const Tags = props => {

    return(
        <div className="Tags">
            {
                props.tags.map((tag, index) => {
                    return (
                        <div className="Tags__item" key={index} style={{backgroundColor: tag.color}}>
                            <span>#{tag.name}</span>
                            <span
                                className="Tags__delete"
                                onClick={() => {props.deleteTag(tag.name)}}
                                > × </span>
                        </div>
                    )
                })
            }

        </div>
    )

}
