import React, { Component } from 'react';
import './App.css';
import {Input} from './Components/Input/Input';
import tags from './tags'

class App extends Component {
    constructor(props){
        super(props);

        this.tags = tags.tags;

        this.state = {
            valueInput: '',
            tags: this.tags,
            addedTags: []
        }

        this.searchTags = this.searchTags.bind(this);
        this.addTags = this.addTags.bind(this);
        this.deleteTag = this.deleteTag.bind(this);
    }

    searchTags(e){
        let searchTag = '';
        let displayedTags = this.tags;

        this.setState({valueInput: e.target.value})

        if(e.target.value.indexOf('+') !== -1 || e.target.value.indexOf('-') !== -1 || e.target.value.indexOf(' ') !== -1){

            var regexp = /[+\- ]/g;
            var result;
            while (result = regexp.exec(e.target.value)) {

                this.lastIndex = regexp.lastIndex
            }

            searchTag = e.target.value.slice(this.lastIndex,e.target.value.length);
            this.searchTag = e.target.value.slice(this.lastIndex-1,e.target.value.length);

            displayedTags = this.tags.filter(tag => {
                return tag.includes(searchTag);
            });

            this.setState({tags: displayedTags});
        }

        else
        {
            searchTag = e.target.value;
            displayedTags = this.tags.filter(tag => {
                return tag.includes(searchTag);
            });

            this.setState({tags: displayedTags});
        }
    }

    addTags(tag){

        let color = 'blue';
        if(this.searchTag.lastIndexOf('+') !== -1) color = 'green'
        if(this.searchTag.lastIndexOf('-') !== -1) color = 'red'

        let newTag = {
            name: tag,
            color: color
        };

        let newAddedTags = this.state.addedTags.slice();

        newAddedTags.push(newTag);

        this.setState({addedTags: newAddedTags})

        this.tags.splice(this.tags.indexOf(tag), 1);
        this.setState({tags: this.tags});

        if(this.state.valueInput.indexOf(' ') !== -1 || this.state.valueInput.indexOf('+') !== -1 || this.state.valueInput.indexOf('-') !== -1) {
            this.searchTag = this.state.valueInput.slice(this.lastIndex,this.state.valueInput);
            this.setState({tags: this.tags, valueInput: this.state.valueInput.slice(0, this.lastIndex-1)});
        }
        else this.setState({tags: this.tags, valueInput: ''});


    }

    deleteTag(tag) {
        let newDeleteTags = this.state.addedTags.slice();

        this.state.addedTags.forEach((item,index) => {
            if(item.name.indexOf(tag) !== -1 && item.name.length === tag.length){
                newDeleteTags.splice(index, 1);
                this.tags.push(item.name);
                this.setState({addedTags: newDeleteTags, tags: this.tags})
            }
        })

    }

  render() {
    return (
      <div className="App">
          <Input
              searchTags={this.searchTags}
              addTags={this.addTags}
              tags={this.state.addedTags}
              value={this.state.valueInput}
              initialTags={this.state.tags}
              deleteTag={this.deleteTag}
            />
      </div>
    );
  }
}

export default App;
